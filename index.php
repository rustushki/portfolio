<html>

<head>
<title>Russ Adams - Portfolio</title>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101984268-1', 'auto');
  ga('send', 'pageview');

</script>

<style>
body {
	padding: 0em;
	margin: 0em;
}

* {
    box-sizing: border-box;
}

[class*="column-"] {
    float: left;
    padding: 1em;
	padding-right: 0em;
}

.content::after {
    content: "";
    clear: both;
    display: block;
}

.column-1 {
	width: 33%;
	display: block;
}

.column-2 {
	width: 33%;
	display: block;
}

.column-3 {
	width: 33%;
	display: block;
}

.portfolio-item {
	background: #AAA;
	padding: 1em;
	margin: 0em;
	margin-bottom: 1em;
	border: 0.20em solid #000; 
}

.portfolio-header {
	margin-top: 0em;
}

.header {
	background: #333;
	padding: 1em;
	color: #eee;
}

.footer {
	background: #333;
	padding: 1em;
	color: #eee;
	box-shadow: 0px 500px 0px 500px #000;
}

</style>
</head>

<body>


<div class="header">
	<h1>Russ Adams</h1>

	<p>Hi. I'm a Software Engineer in the Greensboro, NC area. I enjoy
	challenging programming projects and my experience includes website back ends,
	front ends, middleware, heavy clients and simple games. The purpose of this
	website is to showcase my work both for my own self understanding, and also for
	you to see.</p>
</div>

<div class="content">
	<div class="column-1">
		<div class="portfolio-item">
			<h2 class="portfolio-header">KeyRunner</h2>

			<p>Help Moschata escape certain doom. The clock is ticking and she must
			reach the door with the key before it's too late.  KeyRunner is a free game,
			available in source code with the GPLv2 License. I wrote this game as the first
			in a series of smallish games.<p>

			<a href="http://gitlab.com/rustushki/keyrunner">GitLab</a>
		</div>

		<div class="portfolio-item">
			<h2 class="portfolio-header">JavaImp</h2>

			<p>This is an old Vim plugin that was written to organize Java import
			statements--which is something I needed when started writing Java on a daily
			basis.  This fork optimizes and modernizes the old JavaImp plugin.</p>

			<a href="https://github.com/rustushki/JavaImp.vim">GitHub</a>
		</div>
	</div>

	<div class="column-2">
		<div class="portfolio-item">
			<h2 class="portfolio-header">Last Door</h2>

			<p>Last Door is a piece of interactive fiction which I'm writing in
			order to create a simple text-based adventure engine.  My motivation for this
			project was that I enjoyed writing interactive fiction with Inform 6, but I
			found that (while really neat) Inform 7's natural language compiler was not a
			good fit for me.  As of 5/2017, Last Door is still in active development.  I'm
			working on rudimentary concepts like take-able items, inventory and action
			commands.</p>

			<a href="https://gitlab.com/rustushki/lastdoor">GitLab</a>
		</div>

		<div class="portfolio-item">
			<h2 class="portfolio-header">Doozer</h2>

			<p>Doozer is a plugin which manages source projects in Vim.  It allows
			you to have project-specific editor and compiler settings.</p>

			<a href="https://gitlab.com/rustushki/doozer">GitLab</a>


		</div>
	</div>

	<div class="column-3">
		<div class="portfolio-item">
			<h2 class="portfolio-header">Project Euler Solutions</h2>

			<p>Project Euler is a website community which creates and solves
			math problems.  These problems bump into scale and complexity constraints that
			require analysis, solution modeling, and usually writing a program
			to solve.  My goal is to complete a few of these problems each year, and I
			usually implement the solutions in C++.  My solutions (while not the best) are
			both functional and my own.  They are posted to GitLab.</p>

			<a href="https://gitlab.com/rustushki/euler">GitLab</a>


		</div>

		<div class="portfolio-item">
			<h2 class="portfolio-header">Dotfiles</h2>

			<p>Here are my dot config files.  On a new Linux system, I clone
			this repository and then run 'make'.  The result is that all of my
			configuration files are deployed to their appropriate places.  I wanted these
			dotfiles to be fairly portable so that they work on the last few releases of
			the major Linux distros.  This enables them to run well on most Linux
			hosts that I need to connect with.</p>

			<a href="https://gitlab.com/rustushki/dotfiles">GitLab</a>


		</div>
	</div>
</div>

<div class="footer">
&copy <?php echo date("Y") ?> Russ Adams
</div>

</body>

</html>
